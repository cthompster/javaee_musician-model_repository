package org.harouna.model;

public enum InstrumentType {
	WOOD, BRASS, CORD, KEYBOARD, DRUM
}
