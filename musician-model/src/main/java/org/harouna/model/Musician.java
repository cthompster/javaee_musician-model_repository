package org.harouna.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

//Entit� JPA c'est un bean (@id, @Entity) @Entity �tant la classe
@Entity 
@Table(name = "t_musician") //associe la classe � la table en bdd
public class Musician {

	//Colonne pkid associ� � la propri�t� id
	@Column(name = "pkid")
	@Id @GeneratedValue(strategy = GenerationType.AUTO) //G�n�re automatiquement d'id //@Id pour dire que id est une cl� primaire
	private long id;

	@Column(name = "musician_name", length = 64) 
	private String name;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "date_of_birth")
	private Date dateOfBirth;

	@Enumerated(EnumType.STRING)
	@Column(name = "music_type", length = 15)
	private MusicType musicType;

	@OneToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER) //fetchType permet de pr�charger les relations entre les objets
	@JoinFetch(JoinFetchType.OUTER ) //Une seule requete SELECT par jointure automatiquement
	private Instrument instrument;

	@OneToMany(cascade= CascadeType.PERSIST,
			fetch = FetchType.EAGER,
			mappedBy= "musician")
	private List<Song> songs = new ArrayList<>();
	
	@Embedded 
	private  Adress adress;

	public Adress getAdress() {
		return adress;
	}

	public void setAdress(Adress adress) {
		this.adress = adress;
	}

	public List<Song> getSongs() {
		return songs;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public MusicType getMusicType() {
		return musicType;
	}

	public void setMusicType(MusicType musicType) {
		this.musicType = musicType;
	}
	public Instrument getInstrument() {
		return instrument;
	}

	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}


	@Override
	public String toString() {
		return "Musician [id=" + id + ", name=" + name + ", dateOfBirth=" + dateOfBirth + ", musicType=" + musicType
				+ "]";
	}
}
